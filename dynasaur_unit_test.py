import os
import io
import unittest
import sys
import glob
import argparse

from unittest import TestCase

from dynasaur.plugins.data_visualization_controller import DataVisualizationController
from dynasaur.plugins.criteria_controller import CriteriaController
from dynasaur.data.dynasaur_definitions import DynasaurDefinitions
from dynasaur.utils.logger import ConsoleLogger

import matplotlib.pyplot as plt


class TestImpl(TestCase):
    """
    Init class contructor
    """
    def __init__(self, testname, path):
        super(TestImpl, self).__init__(testname)
        self._path = path



    def test_board_dyna(self):
        ball_board_dyna_dir = os.path.join(self._path, "ball_board_simple_dyna")

        path_to_def_func_def = os.path.join(ball_board_dyna_dir, "func_nodout_data_vis.def")
        path_to_def_id_def = os.path.join(ball_board_dyna_dir, "dyna_id.def")
        path_to_data = os.path.join(ball_board_dyna_dir, "binout*")

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def_func_def,
                                                             object_def_file=path_to_def_id_def,
                                                             data_source=path_to_data)
        commands = [
                    {'visualization': 'BOARD_node_1_vel_z', 'x_label': 'time[ms]', 'y_label': 'y_vel'},
                    {'visualization': 'BOARD_node_2_disp_z', 'x_label': 'time[ms]', 'y_label': 'y_vel'}
                   ]

        for command in commands:
            data_plugin_controller.calculate(command)

    def test_sufehm(self):
        ball_board_dyna_dir = os.path.join(self._path, "binout_sufehm")

        path_to_def_func_def = os.path.join(ball_board_dyna_dir, "calc_procedure.def")
        path_to_def_id_def = os.path.join(ball_board_dyna_dir, "object.def")
        path_to_data = os.path.join(ball_board_dyna_dir, "binout")


        data_vis_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def_func_def,
                                                             object_def_file=path_to_def_id_def,
                                                             data_source=path_to_data)
        commands = [
            {'visualization': 'HEAD_brain_injury_risk_ais2', 'x_label': 'time[ms]', 'y_label': 'y_vel'},
            {'visualization': 'HEAD_brain_max_von_mises', 'x_label': 'time[ms]', 'y_label': 'risk'}

        ]

        for command in commands:
            data_vis_controller.calculate(command)

        print(data_vis_controller.get_data())

        fig, (ax0, ax1) = plt.subplots(2, 1, constrained_layout=True)
        ax0.set_title("SUFEHM - Advanced Head Injury Criterion")

        ax0.plot(data_vis_controller.get_data("HEAD", "brain_max_von_mises")["X"],
                 data_vis_controller.get_data("HEAD", "brain_max_von_mises")["Y"]*1000, color="red")

        ax0.set_ylabel("Brain von Mises Stress [kPa]")
        ax0.set_xlabel("Time [ms]")
        ax0.set_xlim([0, 40])
        ax0.set_ylim([0, 70])
        ax0.grid()
        import numpy as np
        risk_ = data_vis_controller.get_data("HEAD", "brain_injury_risk_ais2")["Y"]*100
        np.argmax(risk_)
        risk_[np.argmax(risk_):] = np.max(risk_)
        ax1.plot(data_vis_controller.get_data("HEAD", "brain_injury_risk_ais2")["X"],
                 risk_, color="red")

        ax1.set_ylabel("Brain injury risk of AIS2+ [%]")
        ax1.set_xlabel("Time [ms]")
        ax1.set_xlim([0, 40])
        ax1.set_ylim([0, 100])
        ax1.grid()


        fig.show()

        crit_controller = CriteriaController(calculation_procedure_def_file=path_to_def_func_def,
                                                             object_def_file=path_to_def_id_def,
                                                             data_source=path_to_data)
        commands = [
                     {'criteria': 'HEAD_max_brain_injury_risk_ais2'},
                   ]

        for command in commands:
            crit_controller.calculate(command)

        print(crit_controller.get_data())

    def test_virtual_3_5_criteria(self):
        """
        testing board vps example
        test of how to implement the vps data in dynasaur
        :return:
        """
        viva_M35_dir = os.path.join(self._path, "binout_VIVA_M35")
        path_to_def_id = os.path.join(viva_M35_dir, "ids.def")
        path_to_data = os.path.join(viva_M35_dir, "binout*")
        path_to_func = os.path.join(viva_M35_dir, "funcs.def")

        assert(os.path.exists(path_to_func))

        criteria_controller = CriteriaController(calculation_procedure_def_file=path_to_func,
                                                 object_def_file=path_to_def_id,
                                                 data_source=path_to_data,
                                                 code_type="LS-DYNA")

        commands = [
            #{'criteria': 'HEAD_func1'},
            #{'criteria': 'HEAD_func2'},
            #{'criteria': 'HEAD_func2'},
            {'criteria': 'MODEL_Hourglass/Internal-Energy_Max'}
        ]

        for command in commands:
            criteria_controller.calculate(command)
        print(criteria_controller.get_data())

    def test_board_simple_vps(self):
        """
        testing board vps example
        test of how to implement the vps data in dynasaur
        :return:
        """
        ball_board_vps_dir = os.path.join(self._path, "ball_board_simple_vps")
        #ball_board_vps_dir = os.path.join(self._path, "ball_board_vps")
        path_to_def_id = os.path.join(ball_board_vps_dir, "vps_ids.def")
        path_to_data = os.path.join(ball_board_vps_dir, "00_Main_Ball_Brett_01_RESULT.erfh5")
        path_to_func = os.path.join(self._path, "ball_board_simple_dyna", "func_nodout_data_vis.def")
        assert(os.path.exists(path_to_func))

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_func,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data,
                                                             code_type="VPS")

        commands = [
            {'visualization': 'BOARD_node_1_vel_z', 'x_label': 'time[ms]', 'y_label': 'y_vel'},
            {'visualization': 'BOARD_node_2_disp_z', 'x_label': 'time[ms]', 'y_label': 'y_vel'},
            {'visualization': 'BOARD_contact_1_force', 'x_label': 'time[ms]', 'y_label': 'Force'},
            {'visualization': 'BOARD_external_work', 'x_label': 'time[ms]', 'y_label': 'Work'}
        ]

        for command in commands:
            data_plugin_controller.calculate(command)

        return

    def compare_dyna_vps_board_example(self):
        """
        testing board vps example
        test of how to implement the vps data in dynasaur
        :return:
        """
        ball_board_vps_dir = os.path.join(self._path, "ball_board_simple_vps")
        path_to_def_id_vps = os.path.join(ball_board_vps_dir, "vps_ids.def")
        path_to_data_vps = os.path.join(ball_board_vps_dir, "00_Main_Ball_Brett_01_RESULT.erfh5")

        ball_board_dyna_dir = os.path.join(self._path, "ball_board_simple_dyna")
        path_to_def_id_dyna = os.path.join(ball_board_dyna_dir, "dyna_id.def")
        path_to_data_dyna = os.path.join(ball_board_dyna_dir, "Convert_to_Dyna_1", "binout*")

        path_to_func = os.path.join(ball_board_dyna_dir, "func_nodout_data_vis.def")
        assert(os.path.exists(path_to_func))


        dpc_dyna = DataVisualizationController(calculation_procedure_def_file=path_to_func,
                                              object_def_file=path_to_def_id_dyna,
                                              data_source=path_to_data_dyna,
                                              code_type="LS-DYNA")

        dpc_vps = DataVisualizationController(calculation_procedure_def_file=path_to_func,
                                                             object_def_file=path_to_def_id_vps,
                                                             data_source=path_to_data_vps,
                                                             code_type="VPS")

        commands = [
            {'visualization': 'BOARD_node_1_vel_z', 'x_label': 'time[ms]', 'y_label': 'y_vel'},
            {'visualization': 'BOARD_node_2_disp_z', 'x_label': 'time[ms]', 'y_label': 'y_vel'},
            {'visualization': 'BOARD_secforc_1_dips', 'x_label': 'time[ms]', 'y_label': 'y_vel'},
            {'visualization': 'BOARD_secforc_2_dips', 'x_label': 'time[ms]', 'y_label': 'y_vel'}
        ]


        for command in commands:
            dpc_vps.calculate(command)
            dpc_dyna.calculate(command)

        plt.plot(dpc_vps.get_data("BOARD", "node_2_disp_z")["X"], dpc_vps.get_data("BOARD", "node_2_disp_z")["Y"], label="VPS", color="red")
        plt.xlabel("time [ms]")
        plt.ylabel("z displacement [mm]")
        plt.legend()

        plt.show()
        plt.plot(dpc_dyna.get_data("BOARD", "node_2_disp_z")["X"], dpc_dyna.get_data("BOARD", "node_2_disp_z")["Y"], label="LS-DYNA" )

        plt.xlabel("time [ms]")
        plt.ylabel("z displacement [mm]")
        plt.legend()
        plt.show()


    def test_beam_mixed_elements(self):
        """
        testing with VIVA cube model
        test of how to handle mixed beam modeling (Zero integration points and multiple intregration points)
        :return:
        """
        beam_dir = os.path.join(self._path, "beam")

        path_to_def = os.path.join(beam_dir, "beam.def")
        path_to_def_id = os.path.join(beam_dir, "id.def")
        path_to_data = os.path.join(beam_dir, "binout*")

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def, object_def_file=path_to_def_id, data_source=path_to_data)
        commands = [
                    {'visualization': 'BONE_ELM_id1', 'x_label': 'atime[ms]', 'y_label': 'smth'},
                    {'visualization': 'BONE_hist_diag1', 'x_label': 'atime[ms]', 'y_label': 'smth'},
                   ]

        for command in commands:
            data_plugin_controller.calculate(command)

        data_plugin_controller.write_CSV(beam_dir)

        crit_controller = CriteriaController(calculation_procedure_def_file=path_to_def, object_def_file=path_to_def_id, data_source=path_to_data)
        commands = [
                    {'criteria': 'VEHICLE_COLLISIONSPEED'},
                    {'criteria': 'VEHICLE_UNIVEXAMPLE2', "t_start": 0.00, "t_end": 0.20},
                    {'criteria': 'VEHICLE_PERCENTILE', "t_start": 0.00, "t_end": 0.20}
                    ]

        for command in commands:
            crit_controller.calculate(command)
        crit_controller.write_CSV(beam_dir)

    def test_basic_ls_functions(self):
        """
        testing with VIVA femur model
        testing injury criteria: writing out single measurement channels and filtering + basic ls_functions like "res",
        "abs", "HIC".
        Compared with Diadem
        :return:
        """
        input_dir = os.path.join(self._path, "binout_VIVA_femur")
        path_to_def = os.path.join(input_dir, "Dynasaur_Def_File_ProtectMe_H350_Filter.def")
        path_to_def_id = os.path.join(input_dir, "Dynasaur_Def_File_ProtectMe_H350_Filter_ID.def")

        path_to_data = os.path.join(input_dir, "binout*")

        crit_controller = CriteriaController(calculation_procedure_def_file=path_to_def, object_def_file=path_to_def_id, data_source=path_to_data)
        commands = [
                    {'criteria': 'HEAD_HIC15'},
                    {'criteria': 'HEAD_BrIC'},
                    {'criteria': 'HEAD_uBrIC'},
                    {'criteria': 'HEAD_HIC36'},
                    {'criteria': 'HEAD_a3ms'},
                    {'criteria': 'HEAD_max_rotation_velocity_rx'},
                    {'criteria': 'HEAD_max_rotation_velocity_ry'},
                    {'criteria': 'HEAD_max_rotation_velocity_rz'}
        ]

        for command in commands:
            crit_controller.calculate(command)
        crit_controller.write_CSV(input_dir)

        data_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def, object_def_file=path_to_def_id, data_source=path_to_data)

        commands = [
                    {'visualization': 'HEAD_acc_x', 'x_label': 'time_in_ms', 'y_label': 'acc_x'},
                    {'visualization': 'HEAD_resultant', 'x_label': 'time_in_ms', 'y_label': 'resultant'},
                    {'visualization': 'HEAD_acc_x_unfiltered', 'x_label': 'time_in_ms', 'y_label': 'acc_x'},
                   ]

        for command in commands:
            data_controller.calculate(command)

        data_controller.write_CSV(input_dir)

    def test_injuries_on_hbm(self):
        """
        testing with VIVA model
        testing injury criteria: HIC, BRIC
        :return:
        """
        # **********************************************************************************************************************

        input_dirs = [os.path.join(self._path, "binout_VIVA_Example"), os.path.join(self._path, "binout_VIVA_Example_recalculated_for_AGU")]

        for input_dir in input_dirs:
            path_to_def = os.path.join(input_dir, "VIRTUAL_VIVA_Dynasaur_Def.def")
            path_to_def_id = os.path.join(input_dir, "VIRTUAL_VIVA_Dynasaur_Def_ID.def")
            path_to_data = os.path.join(input_dir, "binout*")
            crit_controller = CriteriaController(calculation_procedure_def_file=path_to_def, object_def_file=path_to_def_id, data_source=path_to_data)
            commands = [
                        {'criteria': 'HEAD_HIC15'},
                        {'criteria': 'HEAD_BrIC'},
                        {'criteria': 'HEAD_HIC36'},
                        {'criteria': 'NECK_NIC_max'},
                        {'criteria': 'NECK_NIC_risk'}
                        ]

            for command in commands:
                crit_controller.calculate(command)
            crit_controller.write_CSV(input_dir)

            data_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def, object_def_file=path_to_def_id, data_source=path_to_data)

            commands = [
                        {'visualization': 'NECK_NIC_time', 'x_label': 'time_in_ms', 'y_label': 'NIC'},
                       ]

            for command in commands:
                data_controller.calculate(command)
            data_controller.write_CSV(input_dir)

    def test_beam_failed_elements(self):
        """
        testing with battery model
        consists of elout beam elements (single integration point) ... failing over time and get deleted
        """
        input_dir = os.path.join(self._path, "battery")
        path_to_def = os.path.join(input_dir, "battery.def")
        path_to_def_id = os.path.join(input_dir, "battery_id.def")
        path_to_data = os.path.join(input_dir, "binout*")

        data_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def, object_def_file=path_to_def_id, data_source=path_to_data)

        commands = [
                    {'visualization': 'BONE_ELM_id1', 'x_label': 'time_in_ms', 'y_label': 'NIC',  "t_start": 0.00},
                   ]

        for command in commands:
            data_controller.calculate(command)
        diagram_data = data_controller.get_data()


    def test_femur_output_files(self):
        """
       testing with femur model
       testing min, max standard functions and compare the output file with reference files
       """
        path_sim_sim_dir = os.path.join(self._path, "binout_femur")
        path_to_def = os.path.join(path_sim_sim_dir,  "dynasaur.def")
        path_to_def_id = os.path.join(path_sim_sim_dir,  "dynasaur_id.def")
        path_to_data = os.path.join(path_sim_sim_dir,  "binout*")

        criteria_controller = CriteriaController(calculation_procedure_def_file=path_to_def, object_def_file=path_to_def_id,
                                                 data_source=path_to_data)
        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def, object_def_file=path_to_def_id,
                                                             data_source=path_to_data)

        commands = [
            {'criteria': 'HEAD_min_x_coordinate'},
            {'criteria': 'HEAD_max_x_coordinate'},
            {'criteria': 'HEAD_min_x_acceleration'},
            {'criteria': 'HEAD_max_x_acceleration'}
        ]

        data_controller_commands = \
            [
                {'visualization': 'HEAD_X_coordinate', 'x_label': 'time[ms]', 'y_label': 'x[mm]'},
                {'visualization': 'CHEST_x_acceleration', 'x_label': 'time[ms]', 'y_label': 'x[ms]'}
            ]

        for command in commands:
            criteria_controller.calculate(command)
        criteria_controller.write_CSV(path_sim_sim_dir)

        for command in data_controller_commands:
            data_plugin_controller.calculate(command)

        data_plugin_controller.write_CSV(path_sim_sim_dir)

        ref_data_vis = os.path.join(path_sim_sim_dir, "PLUGIN_DATA_VISUALISATION_ref.csv")

        list_of_files_data_vis = glob.glob(path_sim_sim_dir + "\\PLUGIN_DATA_VISUALISATION_*.csv")
        latest_file_data_vis = max(list_of_files_data_vis, key=os.path.getctime)

        self.maxDiff = None
        HelpClass.check_diff_files(ref_data_vis, latest_file_data_vis, self)

        ref_kinematic_crit = os.path.join(path_sim_sim_dir, "PLUGIN_CRITERIA_ref.csv")

        list_of_files_kinematic_crit = glob.glob(path_sim_sim_dir + "\\PLUGIN_CRITERIA_*.csv")
        latest_file_kinematic_crit = max(list_of_files_kinematic_crit, key=os.path.getctime)

        self.maxDiff = None
        HelpClass.check_diff_files(ref_kinematic_crit, latest_file_kinematic_crit, self)

    def test_strain_stress_criteria_hbm(self):
        """
        TODO
        :return:
        """
        # path_sim_sim_dir = os.path.join(self._path, "SolidCube_Parameter_Set_1")
        # path_to_def = os.path.join(path_sim_sim_dir, "Dynasaur_Def_File_ProtectMe_H350_Filter.def")
        # path_to_data = os.path.join(path_sim_sim_dir, "binout*")
        #
        # data_plugin_controller = DataPluginController(calculation_procedure_def_file=path_to_def, data_source=path_to_data)
        # commands = [
        #     {'visualization': 'BONE_ELM_id1', 'x_label': 'atime[ms]', 'y_label': 'smth'},
        #     {'visualization': 'BONE_hist_diag1', 'x_label': 'atime[ms]', 'y_label': 'smth'},
        # ]
        #
        # for command in commands:
        #     data_plugin_controller.run_calculation(command)
        #
        # data_plugin_controller.write_CSV(self._path)
        #
        # crit_controller = CriteriaController(calculation_procedure_def_file=path_to_def, data_source=path_to_data)
        # commands = [
        #     {'criteria': 'VEHICLE_COLLISIONSPEED'},
        #     {'criteria': 'VEHICLE_UNIVEXAMPLE2', "t_start": 0.00, "t_end": 0.20},
        #     {'criteria': 'VEHICLE_PERCENTILE', "t_start": 0.00, "t_end": 0.20}
        # ]
        #
        # for command in commands:
        #     crit_controller.run_calculation(command)
        # crit_controller.write_CSV(self._path)


    def test_method_get_object_data(self):
        path_sim_sim_dir = os.path.join(self._path, "SolidCube_Parameter_Set_1")
        path_to_def = os.path.join(path_sim_sim_dir, "CUBE_SIM_JSON_.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "CUBE_SIM_JSON_ID.def")
        path_to_data = os.path.join(path_sim_sim_dir, "binout*")

        criteria_controller = CriteriaController(calculation_procedure_def_file=path_to_def,
                                                 object_def_file=path_to_def_id, data_source=path_to_data)

        lower_surface_x = criteria_controller.get_object_data(type_="NODE", id_="Lower_Surface", strain_stress=None,
                                                              index=0, channel="x_coordinate", data_offset=(0, -1))

        lower_surface_y = criteria_controller.get_object_data(type_="NODE", id_="Lower_Surface", strain_stress=None,
                                                              index=0, channel="y_coordinate", data_offset=(0, -1))

        print(lower_surface_x.shape)
        print(lower_surface_y.shape)

        import matplotlib.pyplot as plt

        plt.plot(lower_surface_x.flatten(), lower_surface_y.flatten())
        plt.show()

    def test_example_solid_cube(self):
        """
       testin with simple cube model
       testing max functions from nodout, section, glstat, matsum and Part IDs  (ELOUT)  # Stress, Strain Assessment
       """
        path_sim_sim_dir = os.path.join(self._path, "SolidCube_Parameter_Set_1")
        path_to_def = os.path.join(path_sim_sim_dir, "CUBE_SIM_JSON_.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "CUBE_SIM_JSON_ID.def")
        path_to_data = os.path.join(path_sim_sim_dir, "binout*")



        data_controller_commands = \
            [
            {'visualization': 'CUBE_stress_strain_part_max', 'x_label': 'x[mm]', 'y_label': 'z[mm]'},
            {'visualization': 'CUBE_stress_strain_time_history_max', 'x_label': 'x[mm]', 'y_label': 'z[mm]'},
            {'visualization': 'CUBE_x_z', 'x_label': 'x[mm]', 'y_label': 'z[mm]'},
             {'visualization': 'CUBE_z_time', 'x_label': 'time[ms]', 'y_label': 'z[mm]'},
             {'visualization': 'CUBE_Mid_Section_res_force_time', 'x_label': 'time[ms]', 'y_label': 'forcres[kN]'},
             {'visualization': 'MODEL_Total_Energy_time', 'x_label': 'time[ms]', 'y_label': 'E[J]'},
             {'visualization': 'MODEL_Internal_Energy_time', 'x_label': 'x[mm]', 'y_label': 'E[J]'},
             {'visualization': 'CUBE_Internal_Energy_time', 'x_label': 'time[ms]', 'y_label': 'E[J]'},
             {'visualization': 'CUBE_universal', 'x_label': 'atime[ms]', 'y_label': 'smth'}]


        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def, object_def_file=path_to_def_id,
                                                             data_source=path_to_data)
        for command in data_controller_commands:
            data_plugin_controller.calculate(command)

        import matplotlib.pyplot as plt
        plt.plot(data_plugin_controller.get_data()["CUBE"]["stress_strain_time_history_max"]["X"],
                 data_plugin_controller.get_data()["CUBE"]["stress_strain_time_history_max"]["Y"])
        plt.show()

        criteria_controller = CriteriaController(calculation_procedure_def_file=path_to_def, object_def_file=path_to_def_id,
                                                 data_source=path_to_data)


        commands_criteria_controller = [
            {'criteria': 'MODEL_Internal_Energy_Max'},
            {'criteria': 'CUBE_Internal_Energy_Max'},
            {'criteria': 'CUBE_Mid_Section_res_force_max'},
            {'criteria': 'TIME_Calc_Time_max'},
            {'criteria': 'CUBE_Upper_Surface_z_max_displ'},
            {'criteria': 'CUBE_Upper_Surface_y_min'},
            {'criteria': 'CUBE_z_acc_max'},
            {'criteria': 'CUBE_Solid_set_universal_1'},
            {'criteria': 'CUBE_Solid_set_universal_2'},
            {'criteria': 'CUBE_Solid_set_universal_3'}
        ]


        for command in commands_criteria_controller:
            criteria_controller.calculate(command)



        criteria_controller.write_CSV(path_sim_sim_dir)
        data_plugin_controller.write_CSV(path_sim_sim_dir)


    def test_definition_name_incorrect(self):
        """
       test error handling
       testing definition file validator
       """
        path_sim_sim_dir = os.path.join(self._path, "binout_femur")
        path_to_def = os.path.join(path_sim_sim_dir, "dynasaur_exit.def")
        capturedOutput = io.StringIO()  # Create StringIO object
        sys.stdout = capturedOutput
        dynasaur_def = DynasaurDefinitions(ConsoleLogger())
        try:
            dynasaur_def.read_def(path_to_def)
        except AssertionError:
            pass

    def test_invalid_json_file(self):
        """
       test error handling
       testing if tool throw the right error message, invalid json file
       """

        path_sim_sim_dir = os.path.join(self._path, "binout_femur")
        path_to_def = os.path.join(path_sim_sim_dir, "dynasaur_invalid_json.def")
        dynasaur_def = DynasaurDefinitions(ConsoleLogger())
        try:
            dynasaur_def.read_def(path_to_def)
        except SystemExit:
            pass

    def test_csdm_function(self):
        """
       test csdm standard function
       testing if values are same as in reference file
       """
        path_sim_sim_dir = os.path.join(self._path, "binout_CSDM")
        path_to_def_csdm = os.path.join(path_sim_sim_dir, "dynasaur.def")
        path_to_def_csdm_id = os.path.join(path_sim_sim_dir, "dynasaur_ids.def")
        path_to_def_volume = os.path.join(path_sim_sim_dir, "Volume.def")
        path_to_data_csdm = os.path.join(path_sim_sim_dir, "binout*")

        criteria_controller = CriteriaController(calculation_procedure_def_file=path_to_def_csdm,
                                                 object_def_file=path_to_def_csdm_id,
                                                 data_source=path_to_data_csdm, volume_def_file=path_to_def_volume)

        commands_criteria_controller = [{'criteria': 'BRAIN_csdm_ais1+_lim0.02'},
                                        {'criteria': 'BRAIN_csdm_ais3+_lim0.02'},
                                        {'criteria': 'BRAIN_csdm_ais5+_lim0.02'},
                                        {'criteria': 'BRAIN_csdm_lim0.02'},
                                        {'criteria': 'BRAIN_Rcsdm_ais3+_lim0.002'},
                                        {'criteria': 'BRAIN_Rcsdm_ais5+_lim0.002'}]

        for command in commands_criteria_controller:
            criteria_controller.calculate(command)


        criteria_controller.write_CSV(path_sim_sim_dir)
        data = criteria_controller.get_data()

        import glob, pickle
        list_of_input_params = glob.glob(os.path.join(path_sim_sim_dir, "output_*.pkl"))
        dict_params = {}
        for param in list_of_input_params:
            with open(param, "rb") as pkl_file:
                dict_params.update({param: pickle.load(pkl_file)})



        #self.assertEqual(dict_params[list_of_input_params[0]]['csmd'],
        #                 criteria_controller.get_data(criteria_name='csdm_ais1+_lim0.01')['Value'])
        #self.assertEqual(dict_params[list_of_input_params[1]]['csmd'],
        #                 criteria_controller.get_data(criteria_name='csdm_ais3+_lim0.01')['Value'])
        #self.assertEqual(dict_params[list_of_input_params[2]]['csmd'],
        #                 criteria_controller.get_data(criteria_name='Rcsdm_ais3+_lim0.002')['Value'])
        #self.assertEqual(dict_params[list_of_input_params[3]]['csmd'],
        #                 criteria_controller.get_data(criteria_name='Rcsdm_ais5+_lim0.002')['Value'])


    def test_abstat(self):
        path_sim_sim_dir = os.path.join(self._path, "binout_ABSTAT_CPM\\baseline")
        path_to_def = os.path.join(path_sim_sim_dir, "calc.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "ids.def")
        path_to_data = os.path.join(path_sim_sim_dir, "binout*")

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data)

        data_plugin_controller.calculate({"visualization": "AB_abstat_1_vel"})
        print(data_plugin_controller.get_data("AB"))


    def test_single_element(self):
        path_sim_sim_dir = os.path.join(self._path, "single_element")
        path_to_def = os.path.join(path_sim_sim_dir, "func_nodout_data_vis.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "dyna_id.def")
        path_to_data = os.path.join(path_sim_sim_dir, "binout*")

        data_plugin_controller = CriteriaController(calculation_procedure_def_file=path_to_def,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data)

        commands = [{'criteria': "BOARD_solid_obj_1_stress"}]

        for command in commands:
            data_plugin_controller.calculate(command)


        elem = data_plugin_controller.get_object_data(type_="OBJECT", id_="solid_obj_1",
                                                           strain_stress="Strain", index=None, channel=None,
                                                           data_offset=(0, 31))
        #elem["part_data"]
        import numpy as np
        reference_curve = os.path.join(path_sim_sim_dir, "curve.csv")
        my_data = np.genfromtxt(reference_curve, delimiter=' ')
        max_principle = np.max(elem["part_data"][1], axis=2)


        import matplotlib.pyplot as plt
        plt.plot(elem["time"], max_principle.flatten(), label='Dynasaur')
        plt.plot(my_data[:, 0], my_data[:, 1], label='LS-Prepost' )
        plt.grid()
        plt.xlabel("time[ms]")
        plt.ylabel("MPS")
        plt.savefig("xy.png")
        plt.legend()
        plt.show()
        print("xy")



    def test_rib_prob(self):
        path_sim_sim_dir = os.path.join(self._path, "binout_HBM_example_04_Output_2ms_2steps-Rib\\test")
        path_to_def = os.path.join(path_sim_sim_dir, "ribs.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "ribs_ids.def")
        path_to_data = os.path.join(path_sim_sim_dir, "binout*")

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data)


        data_controller_commands = [{'visualization': 'CHEST_R_Ribs_prob_broken_osccar'},
                                    {'visualization': 'CHEST_R_Ribs_prob_broken_forman'},
                                    {'visualization': 'CHEST_R_Ribs_risk'},
                                    {"visualization": "CHEST_L_Ribs_prob_broken"}
                                    ]
        left_ribs = data_plugin_controller.get_object_data(type_="OBJECT", id_="Left Ribs",
                                                           strain_stress="Strain", index=None, channel=None, data_offset=(0, 20))
        first_rib = left_ribs["part_data"][89004201]

        from dynasaur.calc.standard_functions import StandardFunction
        #StandardFunction.surface_strain(left_ribs)

        percentile_value = StandardFunction.percentile(left_ribs, selection_tension_compression="Tension",
                                                       integration_point="Min", percentile=0.75, units=None)

        #StandardFunction.cfc(time=, sampled_array=, units=)

        #return

        for command in data_controller_commands:
            data_plugin_controller.calculate(command)

        data_plugin_controller.write_CSV(path_sim_sim_dir)

        #print(data_plugin_controller.get_data())
        #criteria_controller = CriteriaController(calculation_procedure_def_file=path_to_def, object_def_file=path_to_def_id,
        #                                         data_source=path_to_data)

        #commands_criteria_controller = [{'criteria': 'CHEST_1_rib_prob_25.5_forman'},
        #                                {'criteria': 'CHEST_25.5_forman'},
        #                                {'criteria': 'CHEST_Max_Ribs_prob_broken_forman'},
        #                                ]

        #for command in commands_criteria_controller:
        #    criteria_controller.calculate(command)

        #criteria_controller.write_CSV(path_sim_sim_dir)


    def test_ISO_MME(self):
        path_sim_sim_dir = os.path.join(self._path, "SUV_40kph_AM50_HC")
        path_to_def = os.path.join(path_sim_sim_dir, "dynasaur.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "dynasaur_ids.def")
        path_to_data = os.path.join(path_sim_sim_dir, "binout*")

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def, object_def_file=path_to_def_id,
                                                             data_source=path_to_data)

        data_controller_commands = [{'visualization': 'CONTACT_pedestrianGV', 'x_label': 'F[kN]', 'y_label': 'time[ms]'},
                                    {'visualization': 'CONTACT_headGV', 'x_label': 'F[kN]', 'y_label': 'time[ms]'},
                                    {'visualization': 'CONTACT_armGV', 'x_label': 'F[kN]', 'y_label': 'time[ms]'},
                                    {'visualization': 'CONTACT_rightlegbumper', 'x_label': 'F[kN]', 'y_label': 'time[ms]'},
                                    {'visualization': 'CONTACT_torsobumper', 'x_label': 'F[kN]', 'y_label': 'time[ms]'},
                                    {'visualization': 'CONTACT_pedestrianbumper', 'x_label': 'F[kN]',
                                     'y_label': 'time[ms]'},
                                    {'visualization': 'CONTACT_pedestrianbonnet', 'x_label': 'F[kN]',
                                     'y_label': 'time[ms]'},
                                    {'visualization': 'HC_X_coordinate', 'x_label': 'x[mm]', 'y_label': 'time[ms]'},
                                    {'visualization': 'HC_Z_coordinate', 'x_label': 'z[ms]', 'y_label': 'time[ms]'},
                                    {'visualization': 'C7_X_coordinate', 'x_label': 'x[mm]', 'y_label': 'time[ms]'},
                                    {'visualization': 'C7_Z_coordinate', 'x_label': 'z[ms]', 'y_label': 'time[ms]'},
                                    {'visualization': 'T12_X_coordinate', 'x_label': 'x[mm]', 'y_label': 'time[ms]'},
                                    {'visualization': 'T12_Z_coordinate', 'x_label': 'z[ms]', 'y_label': 'time[ms]'},
                                    {'visualization': 'AC_X_coordinate', 'x_label': 'x[mm]', 'y_label': 'time[ms]'},
                                    {'visualization': 'AC_Z_coordinate', 'x_label': 'z[ms]', 'y_label': 'time[ms]'},
                                    {'visualization': 'T8_X_coordinate', 'x_label': 'x[mm]', 'y_label': 'time[ms]'},
                                    {'visualization': 'T8_Z_coordinate', 'x_label': 'z[ms]', 'y_label': 'time[ms]'},
                                    {'visualization': 'FER_X_coordinate', 'x_label': 'x[mm]', 'y_label': 'time[ms]'},
                                    {'visualization': 'FER_Z_coordinate', 'x_label': 'z[ms]', 'y_label': 'time[ms]'},
                                    {'visualization': 'MR_X_coordinate', 'x_label': 'x[mm]', 'y_label': 'time[ms]'},
                                    {'visualization': 'MR_Z_coordinate', 'x_label': 'z[ms]', 'y_label': 'time[ms]'},
                                    {'visualization': 'FEL_X_coordinate', 'x_label': 'x[mm]', 'y_label': 'time[ms]'},
                                    {'visualization': 'FEL_Z_coordinate', 'x_label': 'z[ms]', 'y_label': 'time[ms]'},
                                    {'visualization': 'ML_X_coordinate', 'x_label': 'x[mm]', 'y_label': 'time[ms]'},
                                    {'visualization': 'ML_Z_coordinate', 'x_label': 'z[ms]', 'y_label': 'time[ms]'},
                                    {'visualization': 'HC_x_acceleration', 'x_label': 'xacc[g]', 'y_label': 'time[ms]'},
                                    {'visualization': 'HC_resultant_acceleration', 'x_label': 'resacc[g]',
                                     'y_label': 'time[ms]'},
                                    {'visualization': 'HC_resultant_velocity', 'x_label': 'resvel[m_s]',
                                     'y_label': 'time[ms]'},
                                    {'visualization': 'MODEL_hourglass_energy', 'x_label': 'En[J]', 'y_label': 'time[ms]'},
                                    {'visualization': 'MODEL_internal_energy', 'x_label': 'En[J]', 'y_label': 'time[ms]'},
                                    {'visualization': 'MODEL_total_energy', 'x_label': 'En[J]', 'y_label': 'time[ms]'},
                                    {'visualization': 'MODEL_contact_energy', 'x_label': 'En[J]', 'y_label': 'time[ms]'},
                                    {'visualization': 'MODEL_added_mass', 'x_label': 'Mass[kg]', 'y_label': 'time[ms]'},
                                    {'visualization': 'MODEL_time_step', 'x_label': 'time[ms]', 'y_label': 'time[ms]'},
                                    {'visualization': 'GV_X_coordinate', 'x_label': 'x[mm]', 'y_label': 'time[ms]'},
                                    {'visualization': 'MODEL_Rel_Added_Mass', 'x_label': 'Mass[pro]',
                                     'y_label': 'time[ms]'},
                                    {'visualization': 'HBM_HBM_total_hourglass_energy', 'x_label': 'En[J]',
                                     'y_label': 'time[ms]'},
                                    {'visualization': 'HBM_HBM_total_added_mass', 'x_label': 'Mass[kg]',
                                     'y_label': 'time[ms]'}]

        for command in data_controller_commands:
            data_plugin_controller.calculate(command)

        data_plugin_controller.write_ISO_MME(path_to_dir=path_sim_sim_dir, test=True)


    def test_madymo_implementation(self):
        madymo_dir = os.path.join(self._path, "madymo")

        path_to_def = os.path.join(madymo_dir, "calc.def")
        path_to_def_id = os.path.join(madymo_dir, "objects_madymo.def")
        path_to_data = os.path.join(madymo_dir, "madymo_out.h5")


        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data,
                                                             code_type="MADYMO")

        data_controller_commands = [{"visualization": "skull_head_center_of_gravity_x", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "T1_center_of_vertebral_body_of_T1_center_x", "x_label": "time[s]", "y_label": "displacement [mm]"}, 
                                    {"visualization": "T8_center_of_vertebral_body_of_T8_center_x", "x_label": "time[s]", "y_label": "displacement [mm]"}, 
                                    {"visualization": "C7_center_of_vertebral_body_of_C7_center_x", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Pelvis_center_of_acetabulum_center_x", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Pelvis_anterior_superior_iliac_spine_center_x", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Pelvis_posterior_superior_iliac_spine_center_x", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Femur_midpoint_of_lateral_and_medial_femoral_epicondyle_left_x", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Calcaneus_center_of_medial_edge_of_posterior_surface_of_calcaneu_left_x", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Radius_radius_styloid_left_x", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Femur_midpoint_of_lateral_and_medial_femoral_epicondyle_right_x", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Calcaneus_center_of_medial_edge_of_posterior_surface_of_calcaneu_right_x", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Radius_radius_styloid_right_x", "x_label": "time[s]", "y_label": "displacement [mm]"},

                                    {"visualization": "skull_head_center_of_gravity_y", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "T1_center_of_vertebral_body_of_T1_center_y", "x_label": "time[s]", "y_label": "displacement [mm]"}, 
                                    {"visualization": "T8_center_of_vertebral_body_of_T8_center_y", "x_label": "time[s]", "y_label": "displacement [mm]"}, 
                                    {"visualization": "C7_center_of_vertebral_body_of_C7_center_y", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Pelvis_center_of_acetabulum_center_y", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Pelvis_anterior_superior_iliac_spine_center_y", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Pelvis_posterior_superior_iliac_spine_center_y", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Femur_midpoint_of_lateral_and_medial_femoral_epicondyle_left_y", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Calcaneus_center_of_medial_edge_of_posterior_surface_of_calcaneu_left_y", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Radius_radius_styloid_left_y", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Femur_midpoint_of_lateral_and_medial_femoral_epicondyle_right_y", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Calcaneus_center_of_medial_edge_of_posterior_surface_of_calcaneu_right_y", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Radius_radius_styloid_right_y", "x_label": "time[s]", "y_label": "displacement [mm]"},
         
                                    {"visualization": "skull_head_center_of_gravity_z", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "T1_center_of_vertebral_body_of_T1_center_z", "x_label": "time[s]", "y_label": "displacement [mm]"}, 
                                    {"visualization": "T8_center_of_vertebral_body_of_T8_center_z", "x_label": "time[s]", "y_label": "displacement [mm]"}, 
                                    {"visualization": "C7_center_of_vertebral_body_of_C7_center_z", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Pelvis_center_of_acetabulum_center_z", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Pelvis_anterior_superior_iliac_spine_center_z", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Pelvis_posterior_superior_iliac_spine_center_z", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Femur_midpoint_of_lateral_and_medial_femoral_epicondyle_left_z", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Calcaneus_center_of_medial_edge_of_posterior_surface_of_calcaneu_left_z", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Radius_radius_styloid_left_z", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Femur_midpoint_of_lateral_and_medial_femoral_epicondyle_right_z", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Calcaneus_center_of_medial_edge_of_posterior_surface_of_calcaneu_right_z", "x_label": "time[s]", "y_label": "displacement [mm]"},
                                    {"visualization": "Radius_radius_styloid_right_z", "x_label": "time[s]", "y_label": "displacement [mm]"},
         

                            ]

        for command in data_controller_commands:
            data_plugin_controller.calculate(command)

        #print(data_plugin_controller.get_data())
        data_plugin_controller.write_CSV(madymo_dir)


class HelpClass():
    @staticmethod
    def check_diff_files(reference_file, current_file, self):
        with open(reference_file) as reference:
            with open(current_file) as output:
                self.maxDiff = None
                self.assertEqual(reference.read(), output.read())


class readable_dir(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        prospective_dir=values
        if not os.path.isdir(prospective_dir):
            raise argparse.ArgumentTypeError("readable_dir:{0} is not a valid path".format(prospective_dir))
        if os.access(prospective_dir, os.R_OK):
            setattr(namespace,self.dest,prospective_dir)
        else:
            raise argparse.ArgumentTypeError("readable_dir:{0} is not a readable dir".format(prospective_dir))


def test_full(path):
    suite = unittest.TestSuite()
    test_loader = unittest.TestLoader()
    test_names = test_loader.getTestCaseNames(TestImpl)

    for test_name in test_names:
        suite.addTest(TestImpl(test_name, path))

    return suite


def test_beam_del_elems_zero_ips(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_beam_failed_elements', path))
    return suite


def test_viva_(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_injuries_on_hbm', path))
    return suite


def test_stress_strain_based_hbm_criteria(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_strain_stress_criteria_hbm', path))
    return suite


def test_viva_femur(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_basic_ls_functions', path))
    return suite


def test_all_data_types(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_example_solid_cube', path))
    return suite


def test_madymo(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_madymo_implementation', path))
    return suite

def test_sufehm(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_sufehm', path))
    return suite


def test_virtual_3_5_criteria(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_example_solid_cube', path))
    return suite


def test_compare_dyna_vps(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('compare_dyna_vps_board_example', path))
    return suite


if __name__ == "__main__":
    path = "."
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--path", action=readable_dir, required=True, help="path to unit test_library")

    args = parser.parse_args()
    suite = unittest.TestSuite()

    test_loader = unittest.TestLoader()
    test_names = test_loader.getTestCaseNames(TestImpl)

    #print(test_names)
    test_names = ['test_abstat']
    # test_names = ['test_single_element']
    #test_names = ['test_rib_prob']
    #test_names = ['test_virtual_3_5_criteria']
    # test_names = ["test_method_get_object_data"]
    # test_names = ['test_beam_mixed_elements']
    #test_names = ['test_example_solid_cube']
    # test_names = ['test_board_simple_vps', 'test_board_dyna']#, 'test_board_vps']
    #test_names = ['test_board_simple_vps']
    #test_names = ['test_board_dyna']
    # test_names = ['compare_dyna_vps_board_example']#, 'test_board_vps']
    #test_names = ["test_example_solid_cube"]
    for test_name in test_names:
        suite.addTest(TestImpl(test_name, args.path))

    # suite = test_madymo(args.path)
    # suite = test_sufehm(args.path)
    # suite = test_viva_(args.path)
    #print(suite)

    result = unittest.TextTestRunner().run(suite)
    sys.exit(not result.wasSuccessful())

