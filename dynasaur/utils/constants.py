from enum import Enum
import datetime


class DefFileConstants(object):
    FUNC_CRITERIA = "criteria"
    FUNC_DATA_VISUALIZATION = "data_visualization"


class IsommeConstants(object):
    CHANNELS = "Channels"
    CHANNEL = "Channel"
    INSTUMENTATION_STANDARD = "Not applicable"
    MODEL_6YO = "6yo"
    MODEL_AM50 = "AM50"

    TIME = "Time"
    CONTACT_FORCE = "Contact Force"
    TRAJECTORY = "Trajectory"
    TRAJECTORIES = "Trajectories"
    HEAD_COG = "HC"
    ENERGY = "Energy"
    ENERGIES = "Energies"
    ADDED_MASS = "Added Mass"
    TIMESTEP = "Timestep"
    GV = "GV"
    ENERGY_HBM = "Energy HBM"
    REL_ADDED_MASS = "Rel. Added Mass"
    ADDED_MASS_HBM = "Added Mass HBM"
    HIT = "HIT"
    WAD = "WAD"

    PEDESTRIAN_GV = "pedestrian-GV"
    HEAD_GV = "head-GV"
    ARM_GV = "arm-GV"
    RIGHT_LEG_BUMPER = "right_leg-bumper"
    TORSO_BUMPER = "torso-bumper"
    PEDESTRIAN_BONNET = "pedestrian-bonnet"
    PEDESTRIAN_BUMPER = "pedestrian-bumper"

    HC = "HC"
    C7 = "C7"
    T12 = "T12"
    AC = "AC"
    T8 = "T8"
    FER = "FER"
    ML = "ML"
    FEL = "FEL"
    MR = "MR"
    TOTAL_HOURGLASS_ENERGY_HBM = "total_hourglass_energy-HBM"
    HBM = "HBM"

    RESULTANT = "resultant"
    RESULTANT_CF = "resultant-CF"
    X_COORDINATE = "x-Coordinate"
    Z_COORDINATE = "z-Coordinate"
    Z_ACCELERATION = "z-acceleration"
    RESULTANT_ACCELERATION = "resultant-acceleration"
    RESULTANT_VELOCITY = "resultant-velocity"
    TOTAL_HOURGLASS_ENERGY = "total_hourglass_energy"
    TOTAL_INTERNAL_ENERGY = "total_internal_energy"
    TOTAL_ENERGY = "total_energy"
    CONTACT_ENERGY = "contact_energy"
    ADDED_MASS_WS = "added_mass-"
    # WS stands for whole setup, the definition in the templates is ambigous, maybe those need to be changed later
    # on because of the "-" at the end which is a mistake
    REL_ADDED_MASS_WS = "relative_added_mass-"
    WHOLE_SETUP = "whole_setup"

    NO_VALUE = "NOVALUE"


class TransducerConstants(object):
    COORDINATE = "Coordinate"
    FORCE = "Force"
    TIME = "Time"
    ACCELERATION = "Acceleration"
    VELOCITY = "Velocity"
    ENERGY = "Energy"
    MASS = "Mass"


class UnitsConstants(object):
    TIME = "time"
    LENGTH = "length"
    WEIGHT = "weight"
    SECOND = "s"
    MILLISECOND = "ms"
    METER = "m"
    MILLIMETER = "mm"
    KILOGRAM = "kg"
    KNEWTON = "kN"
    VELOCITY = "m/s"
    TON = "t"
    ONE = 1
    THOUSAND = 1000
    WEIGHT_DEFAULT = 0.001
    JOULE = "J"


class MadymoConstants:
    CHANNEL_NAME = "channel_name"
    COMP = "COMP"
    Y_VALUES = "Y_VALUES"
    X_VALUES = "X_VALUES"
    IDS = "ids"
    SIGNALS = "signals"


class DefinitionConstants(object):
    CODE = "CODE"
    TITLE = "TITLE"
    UNIT = "UNIT"
    OBJECTS = "OBJECTS"
    DATA_VIS = "DATA VISUALIZATION"
    RISK_FUNCTION = "RISK FUNCTION"
    CRITERIA = "CRITERIA"


class TestConstants:
    ELOUT = "_element"
    DATA_VIS = "_data_vis"
    INJURY_CRIT = "_injury_crit"
    DEFORC = "_discrete"
    NODOUT = "_node"
    OBJECTS = "_objects"
    CONTACT = "_contact"
    RISK_FUN = "_riskfunction"
    SBTOUT = "_seatbelt"
    SECTION = "_section"
    TITLE = "_title"
    UNITS = "_units"


class ObjectConstantsForData:
    ELEMENT = "ELEMENT"
    AIRBAG = "AIRBAG"
    ELEMENTOBJECT = "OBJECT"
    CROSS_SECTION = "CROSS_SECTION"
    DISCRETE = "DISCRETE"
    NODE = "NODE"
    SEAT_BELT = "SEAT_BELT"
    CONTACT = "CONTACT"
    ENERGY_PART = "ENERGY_PART"
    ENERGY_GLOBAL = "ENERGY_GLOBAL"
    DISBOUT = "DISBOUT"
    DISBOUT_PART = "DISBOUT_PART"


class JsonConstants:
    TYPE = "type"
    ID = "id"
    ID_UPPER_CASE = "ID"
    ID_RANGE = "id_range"
    PART_ID = "part_id"
    NAME = "name"
    VALUE = "value"
    FUNCTION = "function"
    PLUGIN = "plugin"
    DEFINITIONS = "definitions"
    DEFINITION = "definition"
    TYPE_OF_CRTITERIA = "type_of_criteria"
    PART_OF = "part_of"
    INFO = "info"
    CRITERIA_KINEMATIC = "kinematic"
    CRITERIA_LOAD = "load"
    CRITERIA_INJURY = "injury"
    CRITERIA_ENERGY = "energy"
    CRITERIA_METADATA = "metadata"
    CRITERIA_TIME = "time"
    CRITERIA_RISK = "risk"    
    PARAM = "param"
    ARRAY = "array"
    LIMITS = "limits"


class PluginsParamDictDef:
    DYNASAUR_JSON = "dynasaur_json"
    START_TIME = "t_start"
    END_TIME = "t_end"
    LIMIT = 'limit'
    NR_LARGESR_EL = 'nr_largest_elements'
    AGE = 'age'
    X_LABEL = "x_label"
    Y_LABEL = "y_label"
    SELECTION_TENSION_COMPRESSION = "selection_tension_compression"
    INTEGRATION_POINT = "integration_point"
    PERCENTILE = "percentile"


class OutputStringForPlugins:
    VALUE = 'Value'
    LIMITS = "Limits"
    ALL_MAX = "allmax"
    FRAC_MAX = "fracmax"
    ALL_FRAC = "allfrac"


class DataPluginConstants:
    TIME = 'time'
    VISUALIZATION = "visualization"
    X = "x"
    Y = "y"


class StandardFunctionsDefinition:
    HIC15 = "HIC_15"
    HIC36 = "HIC_36"
    ERROR_NOT_STANDARD_FUNCTION = " is not a standard or user function"
    FUNCTION_NAME = "function_name"


class LOGConstants(object):
    ERROR = ("[ERROR]", "red")
    WARNING = ("[WARNING]", "magenta")
    READ_BINOUT = ("[READ BINOUT]", "blue")
    READ_MADYMO = ("[READ MADYMO]", "yellow")
    READ_VOLUME = ("[READ VOLUME]", "grey")
    READ_DYNASAUR_DEF = ("[READ DYNASAUR DEF]", "green")
    DATA_PLUGIN = ("[DATA]", "white")
    SCRIPT = ("[CALCULATION]", "cyan")
    INPUT = ("[USER INPUT]", "blue")


class LoggerDefinitionFileValidator:
    ID_MUST_BE_DEFINITED = "ID has to be defied!"
    ID_IS_INTEGER_LIST = "ID has to be a list of integers!"
    DEFINITION_MISSING = "Definition is missing!"
    TITLE_MISSING = "Title value is missing!"
    STRING_TYPE = "Value type has to be a string!"
    TIME_MISSING = "Time has to be defined!"
    LENGTH_MISSING = "Length has to be defined!"
    WEIGHT_MISSING = "Weight has to be defined!"
    RISK_FUN_DEF = "Risk function has to be defined!"
    LIST_TYPE = "Value has to be defined as list!"
    PLUGIN_MISSING = "Plugin has to be defined!"
    LOWER_CASE = "Definition has to be be lowercase!"
    NAME_MISSING = "Name has to be defined!"
    DCDF_CCDF_MISSING = "Risk functions have to be defined as DCDF or CCDF!"
    OBJECT_MISSING = "Objects have to be defined in object!"
    TYPE_MISSING = "object type is missing!"
    PART_OF_MISSING = "\"part_of\" is missing! Definition name: "
    CRITERIA_MISSING = "Criteria has to be defined!"
    CRITERIA_DEFINITION_MISSING = "Criteria has to be a definitions{}"
    CRITERIA_TYPE_OF_MISSING = "Type of criteria has to be defined! Definition name: "
    CRITERIA_FUNCTION_MISSING = "function has to be defined! Definition name: "
    CRITERIA_TYPE_INCORRECT = "Type of criteria can be: load, kinematic or injury. Incorrect:"
    NOT_VALID_DEFINITION = "Invalid DEFINITION: "
    GITLAB_REFERENCE = " check out : https://gitlab.com/VSI-TUGraz/Dynasaur/wikis/definition-file"


class LoggerERROR:
    print_statements = {
        1: " is not supported as data type. Use ELEMENT, CROSS_SECTION, NODE, DISCRETE, SEAT_BELT, CONTACT",
        2: " not available in your binout",
        3: " No file to select",
        4: " JSON object issue: %s",
        5: " Definition does not contain key ",
        6: " In CRITERIA plugin: Definition does not contain key ",
        7: " In DATA VISUALIZATION plugin: Definition does not contain key "
    }


class LoggerWARNING:
    print_statements = {
        1: " is not a valid definition!",
        2: " DATA VISUALIZATION plugin: Definition does not contain header information for the generating ISO-MME files."
           " The information is preset to [" + "NOVALUE , " + datetime.date.today().strftime("%d/%m/%Y") + ", 1, 6yo]"
    }


class LoggerSCRIPT:
    print_statements = {
        1: " writing csv to ",
        2: " writing pdf to ",
        3: " writing channel files to ",
        4: " writing channel summary to ",
        5: " writing isomme to ",
        6: " done writing csv to "
    }


class LoggerReadDynasaurDefinitionFile:
    READ = "read %s"
    DONE = "done"


class VPSDataConstant:
    NODOUT = "nodout"
    NODE = "NODE"
    SECFORC = "secforc"
    SECTION = "SECTION"
    RCFORC = "rcforc"
    CONTACT = "CONTACT"
    GLSTAT = "glstat"
    ENERGY_GLOBAL = "MODEL"
    MATSUM = "matsum"
    ENERGY_PART = "PART"


    #nodout
    COORDINATE = "COORDINATE"
    TRANSLATION_DISPLACEMENT = "Translational_Displacement"
    VELOCITY = "Velocity"
    ACCELERATION = "Acceleration"
    ROTATION_ANGLE = "Rotational_Angle"
    ROTATION_VELOCITY = "Rotational_Velocity"
    ROTATION_ACCELERATION = "Rotational_Acceleration"

    #secforc
    SECTION_CENTRE_POSITION = "Section_Centre_Position"
    SECTION_FORCE = "Section_Force"
    SECTION_MOMENT = "Section_Moment"

    #rcforc
    CONTACT_FORCE = "Contact_Force"

    #glstat
    ENERGY_GLOBAL_EXTERNAL = "TEXT"
    ENERGY_GLOBAL_ENKIT = "ENKIT"

class DataChannelTypesNodout:
    X_COORDINATE = "x_coordinate"
    Y_COORDINATE = "y_coordinate"
    Z_COORDINATE = "z_coordinate"

    X_DISPLACEMENT = "x_displacement"
    Y_DISPLACEMENT = "y_displacement"
    Z_DISPLACEMENT = "z_displacement"

    X_VELOCITY = "x_velocity"
    Y_VELOCITY = "y_velocity"
    Z_VELOCITY = "z_velocity"

    X_ACCELERATION = "x_acceleration"
    Y_ACCELERATION = "y_acceleration"
    Z_ACCELERATION = "z_acceleration"

    RX_DISPLACEMENT = "rx_displacement"
    RY_DISPLACEMENT = "ry_displacement"
    RZ_DISPLACEMENT = "rz_displacement"

    RX_VELOCITY = "rx_velocity"
    RY_VELOCITY = "ry_velocity"
    RZ_VELOCITY = "rz_velocity"

    RX_ACCELERATION = "rx_acceleration"
    RY_ACCELERATION = "ry_acceleration"
    RZ_ACCELERATION = "rz_acceleration"


class DataChannelTypesContact:
    X_FORCE = "x_force"
    Y_FORCE = "y_force"
    Z_FORCE = "z_force"

class DataChannelTypesGlobalEnergy:
    EXTERNAL_WORK = "external_work"
    INTERNAL_ENERGY = "internal_energy"
    KINETIC_ENERGY = "kinetic_energy"
    TOTAL_ENERGY = "total_energy"


class DataChannelTypesSecforc:
    X_CENTROID = "x_centroid"
    Y_CENTROID = "y_centroid"
    Z_CENTROID = "z_centroid"

    X_FORCE = "x_force"
    Y_FORCE = "y_force"
    Z_FORCE = "z_force"
    TOTAL_FORCE = "total_force"

    X_MOMENT = "x_moment"
    Y_MOMENT = "y_moment"
    Z_MOMENT = "z_moment"
    TOTAL_MOMENT = "total_moment"
